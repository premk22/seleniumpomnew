package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	public CreateLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.NAME, using="submitButton") WebElement eleSubmit;
	public CreateLeadPage enterCompanyName() {
		clearAndType(eleCompanyName, "TestLeaf");
		return this;
	}
	
	public CreateLeadPage enterFirstName() {
		clearAndType(eleFirstName, "Name-first");
		return this;
	}
	
	public CreateLeadPage enterLastName() {
		clearAndType(eleLastName, "Name-last");
		return this;
	}
	
	public ViewLeadsPage clickSubmit() {
		click(eleSubmit);
		return new ViewLeadsPage();
	}
}
